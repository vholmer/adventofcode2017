def part1(file):
	res = 0
	for row in file:
		maxi = float("-inf")
		mini = float("inf")
		for word in row.split("\t"):
			n = int(word)
			if n > maxi:
				maxi = n
			if n < mini:
				mini = n
		diff = maxi - mini
		res += diff
	return res

def main():
	file = open("./input")
	res = part1(file)
	print(res)

if __name__ == '__main__':
	main()